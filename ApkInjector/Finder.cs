﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using System.IO;
using System.Text.RegularExpressions;
namespace ApkInjector
{
	class Finder
	{
		readonly XNamespace android;
		readonly XName name;

		XElement _mainActivity;

		/// <summary>
		/// The MainActivity xml element.
		/// </summary>
		public XElement MainActivity { get { return _mainActivity; } }
		
		/// <summary>
		/// Gets the path of the MainActivity. Empty if not found.
		/// </summary>
		public string MainActivityPath { get { 
			if(_mainActivity == null)
				return string.Empty;
			return _mainActivity.Attribute(name).Value.Replace('.', '/') + ".smali";
		} }

		public Finder ()
		{
			android = "http://schemas.android.com/apk/res/android"; // xml vuole che venga specificato il namespace come uri :S
			name = android + "name";
		}

		/// <summary>
		/// Finds the main activity xml element and returns the name attribute.
		/// </summary>
		/// <param name="manifestPath">AndroidManifest.xml file path.</param>
		/// <returns>The name of the starting activity.</returns>
		public string FindMainActivityName(string manifestPath)
		{
			XElement root = XElement.Load(manifestPath);
			IEnumerable<XElement> elements =
				from el in root.Descendants("action")
				where (string)el.Attribute(name) == "android.intent.action.MAIN"
				select el;
			List<XElement> elemList = elements.ToList();
			if (elemList.Count != 0)
			{
				elemList = elemList[0].Ancestors("activity").ToList();
				_mainActivity = elemList[0];
				return _mainActivity.Attribute(name).Value;
			}
			else
			{
				throw new KeyNotFoundException();
			}
		}

		/// <summary>
		/// Returns the line number in MainActivityPath file where smali code can be injected
		/// </summary>
		/// <param name="smaliFolderPath">>The folder containing "smali" folder.</param>
		/// <returns>The line before return statement in onCreate() method.</returns>
		public int FindHijackInjectionLine(string smaliFolderPath)
		{
			int line = 0;
			const string onCreateMethodSignature = ".method protected onCreate*";
			const string returnVoidInstruction = @"\s*return-void";
			if (MainActivityPath == string.Empty)
				throw new FileNotFoundException();
			
			bool found = false;
			using (var fs = File.OpenRead(smaliFolderPath + @"\smali\" + MainActivityPath))
			{
				Regex regexp = new Regex(onCreateMethodSignature);
				StreamReader r = new StreamReader(fs);	
				while (!r.EndOfStream && !found)
				{
					if (regexp.IsMatch(r.ReadLine()))
					{
						regexp = new Regex(returnVoidInstruction);
						while (!r.EndOfStream && !found)
						{	
							if (regexp.IsMatch(r.ReadLine()))
								found = true;
							line++;
						}
					}
					line++;
				}
			}

			return found ? line - 1 : -1; // line before return-void or -1 if not found
		}
	}
}
