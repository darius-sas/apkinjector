﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApkInjector
{
	static class ApkTool
	{
		public static void Run(string apkArgs, TextWriter output = null)
		{
			ProcessStartInfo startInfo = new ProcessStartInfo()
			{
				FileName = "java",
				Arguments = @"-jar .\apktool\apktool.jar -f " + apkArgs,
				CreateNoWindow = true,
				UseShellExecute = false, // Permette di non creare una finestra apposita
				RedirectStandardOutput = true,
			};
			Process apkproc = new Process { StartInfo = startInfo };
			StringBuilder sb = new StringBuilder();
			if (output != null)
			{
				apkproc.OutputDataReceived += new DataReceivedEventHandler(
					delegate(object sender, DataReceivedEventArgs e)
					{
						output.WriteLine(e.Data);
					}
				);
			}

			apkproc.Start();
			apkproc.BeginOutputReadLine();
			apkproc.WaitForExit(); // Ferma gli altri processi
			apkproc.CancelOutputRead();
		}

	}
}
