﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApkInjector
{
	class Program
	{
		static void Main(string[] args)
		{
			Finder finder = new Finder();
			string name = finder.FindMainActivityName(@"G:\Darius\git\C#\ApkInjector\ApkInjector\bin\Debug\apktool\app\AndroidManifest.xml");
			Console.WriteLine("Nome main activity: {0}\nPath: {1}", name, finder.MainActivityPath);
			Console.WriteLine("Inject line: {0}", finder.FindHijackInjectionLine(@"G:\Darius\git\C#\ApkInjector\ApkInjector\bin\Debug\apktool\app\"));
			Console.Read();
		}
	}
}
